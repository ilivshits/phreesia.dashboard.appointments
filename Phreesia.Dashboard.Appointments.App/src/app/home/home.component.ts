import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router'

import { ResourcesService } from '../_services';

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private resourcesService: ResourcesService){ }

    ngOnInit(): void {

        this.route.params
            .map(params => params['practiceId'])
            .subscribe((id) => {
                this.resourcesService.getIntegrationName(id)
                    .subscribe(intName => {
                        this.router.navigateByUrl(`/${intName}/${id}`);
                    });
            });

        const practiceId = this.route.params
    }
}