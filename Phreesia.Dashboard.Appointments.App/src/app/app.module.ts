import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { AppRoutingModule }        from './app-routing.module';
import { UrlSerializer } from '@angular/router'
import { LowerCaseUrlSerializer  } from './_helpers/routerSerializer'

import { AppointmentsService } from './_services/';
import { ResourcesService } from './_services/';

import { HomeComponent } from './home/home.component';
import { AthenaComponent } from './athena/athena.component';
import { NextgenComponent } from './nextgen/nextgen.component';
import {SelectModule} from "ng2-select";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    SelectModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    AthenaComponent,
    NextgenComponent
  ],
  providers: [
    ResourcesService,
    AppointmentsService,
    {
      provide: UrlSerializer,
      useClass: LowerCaseUrlSerializer
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }