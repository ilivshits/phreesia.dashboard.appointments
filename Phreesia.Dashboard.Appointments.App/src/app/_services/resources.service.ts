import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { Department } from '../_models';

@Injectable()
export class ResourcesService {
    constructor(
        private http: Http) {
    }

    getDepartments(): Promise<Department[]> {

        return this.http.get('http://localhost/Appointments.Resource.Service/api/departments')
            .toPromise()
            .then(response =>
                response.json() as Department[]
            )
            .catch(this.handleError);
    }

    getIntegrationName(practiceId: string): Observable<string> {
        return this.http.get(`http://localhost/Appointments.Resource.Service/api/integration/${practiceId}`)
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}