import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { OpenSlot } from '../_models';

@Injectable()
export class AppointmentsService {
    constructor(
        private http: Http) {
    }

    getOpenSlots(): Promise<OpenSlot[]> {

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        // get users from api
        return this.http.post('http://localhost/Appointments.Service/api/appointments/getopenslots', options)
            .toPromise()
            .then(response =>
                response.json() as OpenSlot[]
            )
            .catch(this.handleError);
    }

    schedule(departmentId: number, showFrozenSlots: boolean): Promise<any> {

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        const params = {
            departmentId,
            showFrozenSlots
        };

        // get users from api
        return this.http.post('http://localhost/Appointments.Service/api/appointments/schedule', params, options)
            .toPromise()
            .then(response =>
                response.json()
            )
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}