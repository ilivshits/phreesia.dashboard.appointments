import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AthenaComponent } from './athena/athena.component';
import { NextgenComponent } from './nextgen/nextgen.component';

const routes: Routes = [
  { path: ':practiceId', component: HomeComponent },
  { path: 'athena/:practiceId', component: AthenaComponent },
  { path: 'nextgen/:practiceId', component: NextgenComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
