'use strict';

const path    = require('path');
const helpers = require('./helpers/webpack.helpers');

const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        'config': './src/config/config.js',
        'polyfills': './src/polyfills',
        'vendor': './src/vendor',
        'main': './src/main'
    },
    resolve: {
        extensions: ['.ts', '.js', '.json', '.css', '.html'],
        modules: [
            helpers.root('src'),
            'node_modules'
        ]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: [
                    'ts-loader',
                    'angular2-template-loader'
                ],
                exclude: [/\.(spec|e2e)\.ts$/]
            },
            {
                test: /\.html$/,
                loader: 'raw-loader',
                exclude: [
                    helpers.root('src/index.html')
                ]
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file?name=[name].[ext]'
            },
            {
                test: /\.(less|css)$/,
                exclude: /node_modules/,
                loaders: [
                    'to-string-loader',
                    'css-loader',
                    'autoprefixer-loader?browsers=last 2 version',
                    'less-loader'
                ]
            }
        ]
    },

    plugins: [
        new ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            helpers.root('src')
        ),

        new CommonsChunkPlugin({
            name: ['vendor', 'polyfills', 'config']
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            filename: 'index.html',
            chunks: {
                "body": [
                    "main",'config', 'polyfills', 'vendor'
                ]
            },
            chunksSortMode: 'dependency',
            inject: 'body',
            hash: true
        }),

        new LoaderOptionsPlugin({})
    ]
};
