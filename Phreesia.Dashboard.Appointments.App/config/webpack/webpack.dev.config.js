'use strict';

const merge = require('webpack-merge');
const common = require('./webpack.common.config.js');
const helpers = require('./helpers/webpack.helpers');

const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin');

module.exports = merge(common, {
    devtool: 'cheap-module-source-map',

    output: {
        path: helpers.root('build'),
        filename: '[name].bundle.js',
        sourceMapFilename: '[name].map',
        chunkFilename: '[id].chunk.js',
        library: 'ac_[name]',
        libraryTarget: 'var'
    },

    plugins: [
        new NamedModulesPlugin()
    ],

    devServer: {
        historyApiFallback: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        outputPath: helpers.root('build')
    }
});