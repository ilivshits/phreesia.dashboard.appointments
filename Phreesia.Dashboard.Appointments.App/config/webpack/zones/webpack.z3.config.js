'use strict';

const merge = require('webpack-merge');
const release = require('../webpack.release.config.js');

module.exports = merge(release, {
    entry: {
        'config': './src/config/config.z3.js'
    }
});
