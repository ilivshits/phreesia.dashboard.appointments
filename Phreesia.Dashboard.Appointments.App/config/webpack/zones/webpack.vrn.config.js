'use strict';

const merge = require('webpack-merge');
const release = require('../webpack.release.config');

module.exports = merge(release, {
    entry: {
        'config': './src/config/config.vrn.js'
    }
});
