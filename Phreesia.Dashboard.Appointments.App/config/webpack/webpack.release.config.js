'use strict';

const merge = require('webpack-merge');
const common = require('./webpack.common.config.js');
const helpers = require('./helpers/webpack.helpers.js');

const UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
const NoErrorsPlugin = require('webpack/lib/NoErrorsPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = merge(common, {
    devtool: 'source-map',

    output: {
        path: helpers.root('build'),
        filename: '[name].js',
        sourceMapFilename: '[name].map',
        chunkFilename: '[id].[hash].js'
    },

    plugins: [
        new NoErrorsPlugin(),

        new UglifyJsPlugin({
            sourceMap: false,
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true
            },
            comments: false
        }),

        new CopyWebpackPlugin([
            {
                from: helpers.root('config', 'iis', 'Web.config'), flatten: true
            }
        ])
    ]
});
