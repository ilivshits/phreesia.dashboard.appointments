1) ensure `Node.js` is installed. I have 6.6.0 version (node -v);
2) open terminal from root folder of the project where package.json lies;
3) call `npm i` or `npm install` to install packages defined in package.json;
4) call `npm run dev` to start hosting application on port 5000, the one defined in "dev" script in package.json and manually open it in browser.