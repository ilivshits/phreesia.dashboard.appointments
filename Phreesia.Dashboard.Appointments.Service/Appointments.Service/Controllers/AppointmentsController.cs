﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Appointments.Service.Controllers
{
    [RoutePrefix("api/appointments")]
    public class AppointmentsController : ApiController
    {
        [HttpPost]
        [Route("getopenslots")]
        public List<dynamic> GetOpenSlots()
        {
            return new List<dynamic>()
            {
                new { StartTime = "2017-02-20T21:13:46.097Z", Provider = 150 },
                new { StartTime = "2016-01-30T21:13:46.097Z", Provider = 123 },
                new { StartTime = "2017-11-24T21:13:46.097Z", Provider = 123 },
                new { StartTime = "2015-03-23T21:13:46.097Z", Provider = 150 },
                new { StartTime = "2017-05-11T21:13:46.097Z", Provider = 150 },
            };
        }

        [HttpPost]
        [Route("schedule")]
        public dynamic Schedule(ScheduleModel scheduleModel)
        {
            return
                new
                {
                    success = true,
                    departmentId = scheduleModel.DepartmentId,
                    showFrozenSlots = scheduleModel.ShowFrozenSlots
                };
        }

        public class ScheduleModel
        {
            public int DepartmentId { get; set; }
            public bool ShowFrozenSlots { get; set; }
        }
    }
}
