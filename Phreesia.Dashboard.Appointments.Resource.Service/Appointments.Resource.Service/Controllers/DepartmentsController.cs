﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Appointments.Resource.Service.Controllers
{
    [RoutePrefix("api")]
    public class DepartmentsController : ApiController
    {
        [HttpGet]
        [Route("departments")]
        public List<dynamic> GetDepartments()
        {
            return new List<dynamic>()
            {
                new {Id = 150, DisplayName = "7 Hills"},
                new {Id = 34, DisplayName = "My mind is longing for love"},
                new {Id = 42, DisplayName = "A love my heart just found"},
                new {Id = 213, DisplayName = "Words will not describe!"},
                new {Id = 23532, DisplayName = "The emotions I feel inside"},
                new {Id = 0, DisplayName = "Kidding"},
            };
        }
    }
}
