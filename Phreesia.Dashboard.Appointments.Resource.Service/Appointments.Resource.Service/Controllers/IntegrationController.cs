﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Appointments.Resource.Service.Controllers
{
    [RoutePrefix("api/integration")]
    public class IntegrationController : ApiController
    {
        [HttpGet]
        [Route("{practiceId}")]
        public string GetIntegrationNameByPracticeId(int practiceId)
        {
            if (practiceId > 50)
            {
                return "Athena";
            }
            else
            {
                return "Nextgen";
            }
        }
    }
}
